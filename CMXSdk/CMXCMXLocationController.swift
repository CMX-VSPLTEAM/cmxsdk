//
//  CMXCMXLocationController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


// Content Description : This class deals with anything related to CMX Server

import Foundation

/// Call back handlers , block based
public typealias CMXSCSuccessUpdateBlock = (_ update:CMXLocationData) ->()
public typealias CMXSCFailureUpdateBlock = (_ error:NSError?) ->()

/// Manager class dealling with every type of motion events
class CMXCMXLocationController: NSObject {
    
    /// <#Description#>
    private var updateLocation : CMXSCSuccessUpdateBlock?
    /// <#Description#>
    private var failureUpdateLocation : CMXSCFailureUpdateBlock?
    /// <#Description#>
    private var macAddress = ""
    
    /// initialize instance
    static let shared : CMXCMXLocationController = {
        let instance = CMXCMXLocationController()
        return instance
    }()
    
    override init() {
    }
    
    /// <#Description#>
    public func logOut() {
        stopCMXLocationUpdates()
        clearReferences()
    }
    
    func clearReferences() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.updateLocation = nil
        self.failureUpdateLocation = nil
    }
    
    /// <#Description#>
    ///
    /// - parameter completion: <#completion description#>
    func startCMXLocationUpdates(_ macAddress:String,_ update:@escaping CMXSCSuccessUpdateBlock,failure:@escaping CMXSCFailureUpdateBlock) {
        if CMXAuthentication.shared.isLoggedIn {
            self.macAddress = macAddress
            self.updateLocation = update
            self.failureUpdateLocation = failure
            reScheduleUpdate()
        }else{
            failure(NSError(domain: "Invalid", code: 0, userInfo: ["description":"please login first"]))
        }
    }
    
    /// <#Description#>
    func stopCMXLocationUpdates() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        clearReferences()
    }
    
    /// <#Description#>
    func reScheduleUpdate(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXCMXLocationController.fetchLocationDataFromCMX), object: nil)
        self.perform(#selector(CMXCMXLocationController.fetchLocationDataFromCMX), with: nil, afterDelay: TimeInterval(CMXConfigurations.shared.cmxLocationUpdateInterval))
    }
    
    /// <#Description#>
    func fetchLocationDataFromCMX(){
        
        if !CMXAuthentication.shared.isCMXServerReady() {
            cmxhfLog(data: "Server object is not configured yet , please configure it first before using these services")
            return
        }
        
        
        let information = NSMutableDictionary()
        information.setObject(CMXAuthentication.shared.server!.ip!, forKey: "ip" as NSCopying)
        information.setObject(macAddress, forKey: "macAddress" as NSCopying)
        information.setObject(CMXAuthentication.shared.server!.userName!, forKey: "userName" as NSCopying)
        information.setObject(CMXAuthentication.shared.server!.password!, forKey: "password" as NSCopying)
        CMXCommunication.shared.getUserLocationFromCMX(information) {[weak self] (response) -> () in
            guard let `self` = self else {
                return
            }
            func reportFailure(){
                self.failureUpdateLocation?((NSError(domain: "Invalid", code: 0, userInfo: ["description":"mac address can not be located"])))
            }
            if response != nil {
                if let responseArray = response as? NSArray {
                    if responseArray.count > 0{
                        
                        let info = responseArray[0] as? NSDictionary
                        
                        if let mapCoordinate = info?.object(forKey: "mapCoordinate") as? NSDictionary {
                            
                            let x = "\(mapCoordinate.object(forKey: "x") ?? 0.0)".cmxhfToDouble() ?? 0.0
                            let y = "\(mapCoordinate.object(forKey: "y") ?? 0.0)".cmxhfToDouble() ?? 0.0
                            let z = "\(mapCoordinate.object(forKey: "z") ?? 0.0)".cmxhfToDouble() ?? 0.0
                            let unit = "\(mapCoordinate.object(forKey: "unit") ?? "FEET")"
                            let confidenceFactor = cmxhfSafeString((object:info!.object(forKey: "confidenceFactor")))
                            let mapCoordinate = CMXMapCoordinate(x: x, y: y, z: z, unit: unit)
                            mapCoordinate.confidenceFactor = confidenceFactor
                            
                            if let floorDimension = (info?.object(forKey: "mapInfo") as? NSDictionary)?.object(forKey: "floorDimension") as? NSDictionary {
                                
                                let height = "\(floorDimension.object(forKey: "height") ?? 0.0)".cmxhfToDouble() ?? 0.0
                                let length = "\(floorDimension.object(forKey: "length") ?? 0.0)".cmxhfToDouble() ?? 0.0
                                let offsetX = "\(floorDimension.object(forKey: "offsetX") ?? 0.0)".cmxhfToDouble() ?? 0.0
                                let offsetY = "\(floorDimension.object(forKey: "offsetY") ?? 0.0)".cmxhfToDouble() ?? 0.0
                                let unit = "\(floorDimension.object(forKey: "unit") ?? "FEET")"
                                let width = "\(floorDimension.object(forKey: "width") ?? 0.0)".cmxhfToDouble() ?? 0.0
                                let floorDimension = CMXFloorDimension(height: height, width: width, length: length, offsetX: offsetX, offsetY: offsetY, unit: unit)
                                
                                if let floorImage = (info?.object(forKey: "mapInfo") as? NSDictionary)?.object(forKey: "image") as? NSDictionary {
                                    
                                    let colorDepth = "\(floorImage.object(forKey: "colorDepth") ?? 0)".cmxhfToInt() ?? 0
                                    let height = "\(floorImage.object(forKey: "height") ?? 0)".cmxhfToInt() ?? 0
                                    let imageName = "\(floorImage.object(forKey: "imageName") ?? "")"
                                    let maxResolution = "\(floorImage.object(forKey: "maxResolution") ?? 0)".cmxhfToInt() ?? 0
                                    let size = "\(floorImage.object(forKey: "size") ?? 0)".cmxhfToInt() ?? 0
                                    let width = "\(floorImage.object(forKey: "width") ?? 0)".cmxhfToInt() ?? 0
                                    let zoomLevel = "\(floorImage.object(forKey: "zoomLevel") ?? 0)".cmxhfToInt() ?? 0
                                    let floorImage = CMXFloorImage(colorDepth: colorDepth, height: height, imageName: imageName, maxResolution: maxResolution, size: size, width: width, zoomLevel: zoomLevel)
                                    
                                    if let floorRefId = (info?.object(forKey: "mapInfo") as? NSDictionary)?.object(forKey: "floorRefId") as? String {
                                        
                                        if let serverTimeStamp = (info?.object(forKey: "statistics") as? NSDictionary)?.object(forKey: "currentServerTime") as? String {
                                            
                                            let mapInfo = CMXMapInfo(floorDimension: floorDimension, floorImage: floorImage, floorRefId: floorRefId, serverTimeStamp: serverTimeStamp)
                                            let currentLocation = CMXLocationData(mapInfo: mapInfo, mapCoordinate: mapCoordinate)
                                            self.updateLocation?(currentLocation)
                                            
                                            
                                        }else{ reportFailure() }
                                    }else{ reportFailure() }
                                }else{ reportFailure() }
                            }else{ reportFailure() }
                        }else{ reportFailure() }
                    }else{ reportFailure() }
                }else{ reportFailure() }
            }else{ reportFailure() }
            self.reScheduleUpdate()
        }
    }
}
