//
//  CMXActivityCollection.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

/// To distinguish various types of motion
///
/// - Cycling: motion activity due to cycling
/// - Running: motion activity due to running
/// - Walking: motion activity due to walking
/// - Other:   motion activity due to anything else
public enum ActivityType {
    case cycling
    case running
    case walking
    case other
}

/// an activity
public struct Activity {
    
    let type: ActivityType
    let startDate: Date
    let endDate: Date
    
    /// <#Description#>
    ///
    /// - parameter motionActivity: <#motionActivity description#>
    ///
    /// - returns: <#return value description#>
    init(motionActivity: CMMotionActivity) {
        if motionActivity.cycling {
            type = .cycling
        } else if motionActivity.running {
            type = .running
        } else if motionActivity.walking {
            type = .walking
        } else {
            type = .other
        }
        startDate = motionActivity.startDate as Date
        endDate = motionActivity.startDate as Date
    }
    
    /// <#Description#>
    ///
    /// - parameter activity:   <#activity description#>
    /// - parameter newEndDate: <#newEndDate description#>
    ///
    /// - returns: <#return value description#>
    init(activity: Activity, newEndDate: Date) {
        type = activity.type
        startDate = activity.startDate
        endDate = newEndDate
    }
    
    /// <#Description#>
    ///
    /// - parameter activity: <#activity description#>
    ///
    /// - returns: <#return value description#>
    func appendActivity(_ activity: Activity) -> Activity {
        return Activity(activity: self, newEndDate: activity.endDate)
    }
    
}


/// Collection of activities
class CMXMotionActivityCollection {
    var activities = [Activity]()
    
    /// <#Description#>
    ///
    /// - parameter activities: <#activities description#>
    ///
    /// - returns: <#return value description#>
    init(activities: [CMMotionActivity]) {
        addMotionActivities(activities)
    }
    
    /// <#Description#>
    ///
    /// - parameter motionActivity: <#motionActivity description#>
    func addMotionActivity(_ motionActivity: CMMotionActivity) {
        var activity = Activity(motionActivity: motionActivity)
        if activities.last?.type == activity.type {
            activity = activities.last!.appendActivity(activity)
            activities.removeLast()
        }
        if (activity.type != .other) {
            activities.append(activity)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter motionActivities: <#motionActivities description#>
    func addMotionActivities(_ motionActivities: [CMMotionActivity]) {
        for activity in motionActivities {
            addMotionActivity(activity)
        }
    }
}


