//
//  CMXSdk.h
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

#import <UIKit/UIKit.h>

//! Project version number for CMXSdk.
FOUNDATION_EXPORT double CMXSdkVersionNumber;

//! Project version string for CMXSdk.
FOUNDATION_EXPORT const unsigned char CMXSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CMXSdk/PublicHeader.h>


