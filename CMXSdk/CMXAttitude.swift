//
//  CMXAttitude.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold attitude
public class CMXAttitude: CMXCapturableData {
    
    /// <#Description#>
    public var yaw = 0.0
    /// <#Description#>
    public var roll = 0.0
    /// <#Description#>
    public var pitch = 0.0
    
    /// Init
    ///
    /// - parameter yaw:        yaw value
    /// - parameter roll:       roll value
    /// - parameter pitch:      pitch value
    ///
    /// - returns: return value description
    init(yaw:Double,roll:Double,pitch:Double) {
        self.yaw = yaw
        self.roll = roll
        self.pitch = pitch
    }
}
