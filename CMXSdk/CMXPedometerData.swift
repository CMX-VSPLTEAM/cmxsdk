//
//  CMXPedometerData.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

/// Data modal to hold pedometer
public class CMXPedometerData: CMXCapturableData {
    
    /// <#Description#>
    public var pedometer : CMPedometerData!
    
    /// <#Description#>
    ///
    /// - parameter pedometer:  <#pedometer description#>
    /// - parameter capturedAt: <#capturedAt description#>
    ///
    /// - returns: <#return value description#>
    init(pedometer:CMPedometerData) {
        self.pedometer = pedometer
    }
}
