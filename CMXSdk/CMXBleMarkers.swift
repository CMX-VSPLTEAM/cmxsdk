//
//  CMXBleMarkers.swift
//
//  Created by  on 19/10/16
//  Copyright (c) . All rights reserved.
//

import Foundation

public class CMXBleMarkers: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCMXBleMarkersUUidKey: String = "uUid"
	internal let kCMXBleMarkersZFtKey: String = "zFt"
	internal let kCMXBleMarkersMajorIdKey: String = "majorId"
	internal let kCMXBleMarkersApNameKey: String = "apName"
	internal let kCMXBleMarkersMinorIdKey: String = "minorId"
	internal let kCMXBleMarkersAesUidKey: String = "aesUid"
	internal let kCMXBleMarkersYFtKey: String = "yFt"
	internal let kCMXBleMarkersXFtKey: String = "xFt"


    // MARK: Properties
	public var uUid: String?
	public var zFt: Int?
	public var majorId: Int?
	public var apName: String?
	public var minorId: Int?
	public var aesUid: Int?
	public var yFt: Float?
	public var xFt: Float?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		uUid = json[kCMXBleMarkersUUidKey].string
		zFt = json[kCMXBleMarkersZFtKey].int
		majorId = json[kCMXBleMarkersMajorIdKey].int
		apName = json[kCMXBleMarkersApNameKey].string
		minorId = json[kCMXBleMarkersMinorIdKey].int
		aesUid = json[kCMXBleMarkersAesUidKey].int
		yFt = json[kCMXBleMarkersYFtKey].float
		xFt = json[kCMXBleMarkersXFtKey].float

    }

    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    required public init?(map: Map){

    }

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
		uUid <- map[kCMXBleMarkersUUidKey]
		zFt <- map[kCMXBleMarkersZFtKey]
		majorId <- map[kCMXBleMarkersMajorIdKey]
		apName <- map[kCMXBleMarkersApNameKey]
		minorId <- map[kCMXBleMarkersMinorIdKey]
		aesUid <- map[kCMXBleMarkersAesUidKey]
		yFt <- map[kCMXBleMarkersYFtKey]
		xFt <- map[kCMXBleMarkersXFtKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if uUid != nil {
			dictionary.updateValue(uUid! as AnyObject, forKey: kCMXBleMarkersUUidKey)
		}
		if zFt != nil {
			dictionary.updateValue(zFt! as AnyObject, forKey: kCMXBleMarkersZFtKey)
		}
		if majorId != nil {
			dictionary.updateValue(majorId! as AnyObject, forKey: kCMXBleMarkersMajorIdKey)
		}
		if apName != nil {
			dictionary.updateValue(apName! as AnyObject, forKey: kCMXBleMarkersApNameKey)
		}
		if minorId != nil {
			dictionary.updateValue(minorId! as AnyObject, forKey: kCMXBleMarkersMinorIdKey)
		}
		if aesUid != nil {
			dictionary.updateValue(aesUid! as AnyObject, forKey: kCMXBleMarkersAesUidKey)
		}
		if yFt != nil {
			dictionary.updateValue(yFt! as AnyObject, forKey: kCMXBleMarkersYFtKey)
		}
		if xFt != nil {
			dictionary.updateValue(xFt! as AnyObject, forKey: kCMXBleMarkersXFtKey)
		}

        return dictionary
    }

}
