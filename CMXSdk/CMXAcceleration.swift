//
//  CMXAcceleration.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold accelerate
public class CMXAcceleration: CMXCapturableData {
    
    /// <#Description#>
    public var x = 0.0
    /// <#Description#>
    public var y = 0.0
    /// <#Description#>
    public var z = 0.0
    
    /// Init
    ///
    /// - parameter x:          x value
    /// - parameter y:          y value
    /// - parameter z:          z value
    ///
    /// - returns: newly create object
    init(x:Double,y:Double,z:Double) {
        self.x = x
        self.y = y
        self.z = z
    }
}
