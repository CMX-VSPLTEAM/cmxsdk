//
//  CMXAuthentication.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


// Content Description : This class deals with authentication to CMX Server

import Foundation

/// Call back handlers , block based
public typealias CMXASuccessBlock = (_ server:CMXServer?) ->()
public typealias CMXAFailureBlock = (_ error:NSError?) ->()

/// Manager class dealling with authentication
class CMXAuthentication: NSObject {
    
    private var completionLogin : CMXASuccessBlock?
    /// <#Description#>
    private var failureLogin : CMXAFailureBlock?
    /// <#Description#>
    var server : CMXServer?

    var lastServerCached : CMXServer?

    var isLoggedIn = false
    
    /// initialize instance
    static let shared : CMXAuthentication = {
        let instance = CMXAuthentication()
        return instance
    }()
    
    override init() {
    }
    
    /// authenticate against CMX server
    ///
    /// - parameter ip:         ip address of server
    /// - parameter name:       name (it can be any name, user internally for tagging)
    /// - parameter userName:   userName to login with
    /// - parameter password:   password to authenticate login
    /// - parameter completion: completion callback
    /// - parameter failure:    failure callback
    public func authenticate(ip:String,name:String,userName:String,password:String,completion:@escaping CMXASuccessBlock,failure:@escaping CMXAFailureBlock) {
        self.server = CMXServer(ip: ip, name: name, userName: userName, password: password)
        self.completionLogin = completion
        self.failureLogin = failure
        loadServerDetailsAndServices()
    }
    
    /// <#Description#>
    public func logOut() {
        clearReferences()
        self.server = nil
    }
    
    func clearReferences() {
        self.completionLogin = nil
        self.failureLogin = nil
    }
    
    func isCMXServerReady()->Bool {
        if server == nil || server?.ip == nil || server?.userName == nil || server?.password == nil || server?.name == nil {
            return false
        }
        return true
    }
    
    /// <#Description#>
    func loadServerDetailsAndServices() {
        if !isCMXServerReady() { cmxhfLog(data: "Server object is not configured yet , please configure it first before using these services");return}
        if lastServerCached != nil {
            if lastServerCached!.ip! == server!.ip! && lastServerCached!.userName! == server!.userName && lastServerCached!.password! == server!.password! {
                self.server = lastServerCached
                self.isLoggedIn = true
                self.completionLogin?(self.server)
            }
        }

        let information = NSMutableDictionary()
        information.setObject(server!.name!, forKey: "name" as NSCopying)
        information.setObject(server!.ip!, forKey: "ip" as NSCopying)
        information.setObject(server!.userName!, forKey: "userName" as NSCopying)
        information.setObject(server!.password!, forKey: "password" as NSCopying)
        CMXCommunication.shared.getCMXServerServices(information, completionBlock: { [weak self] (response) in guard let `self` = self else { return }
            if cmxhfIsNotNull(response){
                func updateServerObjectWithServices(services:[NSDictionary]?) {
                    self.server?.services.removeAll()
                    if services != nil {
                        for service in services! {
                            let dnsName = cmxhfSafeString(service.object(forKey: "dnsName"))
                            let lifeCycleStatus = cmxhfSafeString(service.object(forKey: "lifeCycleStatus"))
                            let nodeName = cmxhfSafeString(service.object(forKey: "nodeName"))
                            let port = cmxhfSafeString(service.object(forKey: "port"))
                            let properties = cmxhfSafeString(service.object(forKey: "properties"))
                            let serviceName = cmxhfSafeString(service.object(forKey: "serviceName"))
                            let type = cmxhfSafeString(service.object(forKey: "type"))
                            let weight = cmxhfSafeString(service.object(forKey: "weight"))
                            let service = CMXService(dnsName: dnsName, lifeCycleStatus: lifeCycleStatus, nodeName: nodeName, port: port, properties: properties, serviceName: serviceName, type: type, weight: weight)
                            self.server?.services.append(service)
                        }
                    }
                }
                updateServerObjectWithServices(services: response as? [NSDictionary])
                CMXCommunication.shared.getCMXServerVersion(information, completionBlock: {[weak self] (response) in guard let `self` = self else { return }
                    if cmxhfIsNotNull(response){
                        func updateServerObjectWithVersionInformation(versionInfo:NSDictionary?) {
                            if versionInfo != nil {
                                var cmxImageVersion = cmxhfSafeNSString(versionInfo!["cmx_image_version"])
                                cmxImageVersion = cmxImageVersion.replacingOccurrences(of: "CISCO_CMX-", with: "") as NSString
                                cmxImageVersion = cmxImageVersion.replacingOccurrences(of: ".cmx", with: "") as NSString
                                self.server?.version = cmxImageVersion as String
                            }
                        }
                        if let responseDictionary = response as? NSDictionary {
                            updateServerObjectWithVersionInformation(versionInfo: responseDictionary)
                        }
                        CMXCommunication.shared.checkIfCMXServerValid(information, completionBlock: {[weak self] (response) in guard let `self` = self else { return }
                            if cmxhfIsNotNull(response){
                                self.server?.status = ServerStatus.Running
                                self.lastServerCached = self.server
                                self.isLoggedIn = true
                                self.completionLogin?(self.server)
                            }else{
                                self.lastServerCached = nil
                                self.isLoggedIn = false
                                self.failureLogin?(NSError(domain: "Invalid", code: 0, userInfo: ["description":"server not found"]))
                            }
                        })
                    }else{
                        self.lastServerCached = nil
                        self.isLoggedIn = false
                        self.failureLogin?(NSError(domain: "Invalid", code: 0, userInfo: ["description":"server not found"]))
                    }
                })
            }else{
                self.lastServerCached = nil
                self.isLoggedIn = false
                self.failureLogin?(NSError(domain: "Invalid", code: 0, userInfo: ["description":"server not found"]))
            }
        })
    }
}
