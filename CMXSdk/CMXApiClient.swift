//
//  CMXApiClient.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */



//MARK: - CMXApiClient : This class handles communication of application with its Server.

import Foundation
import AFNetworking

/// Call back handlers , block based
public typealias CMXACCompletionBlock = (_ responseData :AnyObject?) ->()
public typealias CMXACCompletionBlockForFile = (_ responseData :NSData?) ->()

//MARK: - Private
class CMXApiClient: NSObject {
    
    /// <#Description#>
    var completionBlock: CMXACCompletionBlock?
    /// <#Description#>
    var returnFailureResponseAlso = false
    /// <#Description#>
    var returnJsonReponse = false
    /// <#Description#>
    var authenticationUserName : String?
    /// <#Description#>
    var authenticationPassword : String?
    
    /// Check for cache and return from cache if possible.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: bool (wether cache was used or not)
    func loadFromCacheIfPossible(_ body:NSDictionary? , urlString:NSString? ,completionBlock: CMXACCompletionBlock , maxAgeInSeconds:Float)->(Bool){
        return false
    }
    
    /// <#Description#>
    ///
    /// - parameter manager: <#manager description#>
    func updateSecurityPolicy(_ manager:AFHTTPSessionManager){
        let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.none)
        securityPolicy.validatesDomainName = false
        securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy = securityPolicy
    }
    
    /// <#Description#>
    ///
    /// - parameter manager: <#manager description#>
    func addAuthenticationInformationInHeader(_ manager:AFHTTPSessionManager) {
        if (authenticationUserName != nil) && (authenticationPassword != nil){
            manager.requestSerializer.setAuthorizationHeaderFieldWithUsername(authenticationUserName!, password: authenticationPassword!)
        }
    }
    
    /// Perform  GET Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performGetRequest(_ body:NSDictionary? , urlString:AnyObject? ,completionBlock: @escaping CMXACCompletionBlock,methodName:String)->(){
        DispatchQueue.global().async {
            let url = URL(string: urlString as! String)
            cmxhfLog(data: "\n\n\n                  HITTING URL\n\n \(url!.absoluteString)\n\n\n                  WITH GET REQUEST\n\n\(body ?? nil)\n\n" )
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.get(urlString as! String, parameters: body, progress: {[weak self] (progress) in guard let `self` = self else { return }
                }, success: {[weak self] (urlSessionDataTask, responseObject) in guard let `self` = self else { return }
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: {[weak self] (urlSessionDataTask, error) in guard let `self` = self else { return }
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
                    cmxhfLogErrorIfRequired(error: error as NSError?)
            })
        }
    }
    
    /// Perform Get Request For Downloading file data.
    ///
    /// - parameter url: url to download file
    /// - returns: fileData via completionBlock
    func performDownloadGetRequest(_ urlString:NSString? ,completionBlock: @escaping CMXACCompletionBlockForFile,methodName:String)->(){
        DispatchQueue.global().async {
            let url = URL(string: urlString as! String)
            cmxhfLog(data: "\n\n\n                  HITTING URL TO DOWNLOAD FILE\n\n \((url!.absoluteString))\n\n\n" )
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.get(urlString as! String, parameters: nil, progress: {[weak self] (progress) -> Void in guard let `self` = self else { return }
                }, success: {[weak self] (urlSessionDataTask, responseObject) -> Void in guard let `self` = self else { return }
                    completionBlock(responseObject as! NSData?)
            }){(urlSessionDataTask, error) -> Void in
            }
        }
    }
    
    /// Perform Json Encoded Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performJsonPostRequest(_ body:NSDictionary? , urlString:NSString ,completionBlock: @escaping CMXACCompletionBlock,methodName:String)->(){
        DispatchQueue.global().async {
            let url = URL(string: urlString as String)
            cmxhfLog(data: "\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST JSON BODY\n\n\(body ?? nil)\n\n" )
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFJSONRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.post(urlString as String, parameters: body, progress: {[weak self] (progress) in
                }, success: {[weak self] (urlSessionDataTask, responseObject) in guard let `self` = self else { return }
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: {[weak self] (urlSessionDataTask, error) in guard let `self` = self else { return }
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            })
        }
    }
    
    /// Perform Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performPostRequest(_ body:NSDictionary? , urlString:NSString? ,completionBlock: @escaping CMXACCompletionBlock ,methodName:NSString?)->(){
        DispatchQueue.global().async {
            let url = URL(string: urlString as! String)
            cmxhfLog(data: "\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(body ?? nil)\n\n" )
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.post(urlString as! String, parameters: body, progress: {[weak self] (progress) in  guard let `self` = self else { return }
                }, success: {[weak self] (urlSessionDataTask, responseObject) in guard let `self` = self else { return }
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: {[weak self] (urlSessionDataTask, error) in guard let `self` = self else { return }
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            })
        }
    }
    
    /// Add commonly used parameters to all request.
    ///
    /// - parameter information: pass the dictionary object here that you created to hold parameters required. This function will add commonly used parameter into it.
    func addCommonInformation(_ information:NSMutableDictionary?)->(){
    }
    
    /// <#Description#>
    ///
    /// - parameter requestSerialiser: <#requestSerialiser description#>
    func addCommonInformationInHeader(_ requestSerialiser:AFHTTPRequestSerializer?)->(){
    }
    
    /// To check wether the server operation succeeded or not.
    /// - returns: bool
    func isSuccess(_ response:AnyObject?)->(Bool){
        if response != nil{
            return true
        }
        return false
    }
    
    /// To verify the server response received and perform action on basis of that.
    /// - parameter response: data received from server in the form of NSData
    func verifyServerResponse(_ response:AnyObject?,error:NSError?,completionBlock: CMXACCompletionBlock?,methodName:NSString?)->(){
        if error != nil {
            cmxhfPrintErrorMessage(error, methodName: #function)
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }
        else if response != nil {
            DispatchQueue.global().async {
                let responseObject = cmxhfParsedJsonFrom(response as? Data,methodName: methodName!)
                if cmxhfIsNotNull(responseObject) {
                    if self.isSuccess(responseObject){
                        DispatchQueue.main.async(execute: {
                            if self.returnJsonReponse {
                                completionBlock!(NSString(data: response as! Data,encoding: String.Encoding.utf8.rawValue))
                            }else{
                                completionBlock!(responseObject)
                            }
                        })
                    }
                    else {
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                if self.returnJsonReponse {
                                    completionBlock!(NSString(data: response as! Data,encoding: String.Encoding.utf8.rawValue))
                                }else{
                                    completionBlock!(responseObject)
                                }
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock!(nil)
                            })
                        }
                    }
                }
                else {
                    DispatchQueue.main.async(execute: {
                        completionBlock!(nil)
                    })
                }
            }
        }
        else {
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }
    }
}

