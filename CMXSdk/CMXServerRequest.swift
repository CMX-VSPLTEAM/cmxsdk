//
//  CMXServerRequest.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */



//MARK: - CMXServerRequest : This class handles communication of application with its Server.

import Foundation

//MARK: - Url's
struct ServerSupportURLS {
    static let CHECK_CMX_SERVER_VALIDITY = "/api/config/v1/alerts/count"
    static let GET_CMX_SERVER_SERVICES = "/api/config/v1/services"
    static let GET_CMX_SERVER_VERSION = "/api/config/v1/version/image"
    static let GET_USER_LIVE_LOCATION = "/api/location/v2/clients"
    static let GET_USER_MAP = "/api/location/v2/clients"
    static let GET_MAP_FLOOR_PATH = "/api/location/v2/clients"
}

class CMXServerRequest : CMXApiClient {
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func checkIfCMXServerValid(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.CHECK_CMX_SERVER_VALIDITY) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerServices(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_CMX_SERVER_SERVICES) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerVersion(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_CMX_SERVER_VERSION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getUserLocationFromCMX(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            cmxhfCopyData(information, sourceKey: "macAddress", destinationDictionary: parameters, destinationKey: "macAddress", methodName:#function)
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_USER_LIVE_LOCATION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getImageData(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlockForFile) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String , let url = information.object(forKey: "url") as? NSString {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performDownloadGetRequest(url, completionBlock: completionBlock, methodName: #function)
        }
    }
    
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getMapFloorPath(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        completionBlock("{\"version\":\"1\",\"angleOfXAxisToTrueNorth\":209.76476295170949,\"aesUid\":730297895206534100,\"nodes\":[{\"nodeId\":0,\"location\":{\"x\":5.58208955,\"y\":165.64051095}},{\"nodeId\":1,\"location\":{\"x\":29.30597015,\"y\":166.33941606}},{\"nodeId\":2,\"location\":{\"x\":48.84328358,\"y\":164.94160584}},{\"nodeId\":3,\"location\":{\"x\":48.14552239,\"y\":145.37226277}},{\"nodeId\":4,\"location\":{\"x\":47.44776119,\"y\":110.4270073}},{\"nodeId\":5,\"location\":{\"x\":46.75,\"y\":87.36313869}},{\"nodeId\":6,\"location\":{\"x\":45.35447761,\"y\":66.3959854}},{\"nodeId\":7,\"location\":{\"x\":50.93656716,\"y\":38.43978102}},{\"nodeId\":8,\"location\":{\"x\":53.02985075,\"y\":10.48357664}},{\"nodeId\":9,\"location\":{\"x\":27.91044776,\"y\":9.08576642}},{\"nodeId\":10,\"location\":{\"x\":8.37313433,\"y\":9.08576642}},{\"nodeId\":11,\"location\":{\"x\":8.37313433,\"y\":36.34306569}},{\"nodeId\":12,\"location\":{\"x\":8.37313433,\"y\":65.69708029}},{\"nodeId\":13,\"location\":{\"x\":4.88432836,\"y\":88.0620438}},{\"nodeId\":14,\"location\":{\"x\":6.97761194,\"y\":108.33029197}},{\"nodeId\":15,\"location\":{\"x\":6.27985075,\"y\":129.99635036}},{\"nodeId\":16,\"location\":{\"x\":6.27985075,\"y\":148.86678832}},{\"nodeId\":17,\"location\":{\"x\":29.30597015,\"y\":143.97445255}},{\"nodeId\":18,\"location\":{\"x\":30.00373134,\"y\":110.4270073}},{\"nodeId\":19,\"location\":{\"x\":27.21268657,\"y\":66.3959854}}],\"segments\":[{\"startEndNodes\":[0,1],\"angleFromXAxis\":181.68744460878634},{\"startEndNodes\":[1,2],\"angleFromXAxis\":175.90770796019433},{\"startEndNodes\":[2,3],\"angleFromXAxis\":87.95793645790599},{\"startEndNodes\":[3,4],\"angleFromXAxis\":88.85611195188568},{\"startEndNodes\":[4,5],\"angleFromXAxis\":88.26713455842832},{\"startEndNodes\":[5,6],\"angleFromXAxis\":86.192149372436},{\"startEndNodes\":[6,7],\"angleFromXAxis\":101.29189721272866},{\"startEndNodes\":[7,8],\"angleFromXAxis\":94.28215931437296},{\"startEndNodes\":[8,9],\"angleFromXAxis\":3.1850324372373393},{\"startEndNodes\":[9,10],\"angleFromXAxis\":0},{\"startEndNodes\":[10,11],\"angleFromXAxis\":270},{\"startEndNodes\":[11,12],\"angleFromXAxis\":270},{\"startEndNodes\":[12,13],\"angleFromXAxis\":278.8663550728748},{\"startEndNodes\":[13,14],\"angleFromXAxis\":264.103457453039},{\"startEndNodes\":[14,15],\"angleFromXAxis\":271.84458831661306},{\"startEndNodes\":[15,16],\"angleFromXAxis\":270},{\"startEndNodes\":[16,0],\"angleFromXAxis\":272.3820434181862},{\"startEndNodes\":[1,17],\"angleFromXAxis\":90},{\"startEndNodes\":[17,18],\"angleFromXAxis\":91.19153656695534},{\"startEndNodes\":[14,18],\"angleFromXAxis\":185.20289800250893},{\"startEndNodes\":[18,4],\"angleFromXAxis\":180},{\"startEndNodes\":[12,19],\"angleFromXAxis\":182.12457055697158},{\"startEndNodes\":[19,6],\"angleFromXAxis\":180}],\"bleMarkers\":[{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP088\",\"xFt\":111.4,\"yFt\":117.8,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":88},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP089\",\"xFt\":129,\"yFt\":59.6,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":89},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP090\",\"xFt\":4.08645,\"yFt\":170.7,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":90},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP091\",\"xFt\":48.2,\"yFt\":170.7,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":91},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP092\",\"xFt\":81.3,\"yFt\":170.7,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":92},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP093\",\"xFt\":5.4,\"yFt\":90.7,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":93},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP094\",\"xFt\":44.5,\"yFt\":65.3,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":94},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP095\",\"xFt\":47.3,\"yFt\":122.7,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":95},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP096\",\"xFt\":75,\"yFt\":83.6,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":96},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP097\",\"xFt\":4,\"yFt\":8.3,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":97},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP098\",\"xFt\":39,\"yFt\":7.9,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":98},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP099\",\"xFt\":73.7,\"yFt\":7.9,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":99},{\"aesUid\":730297895206534100,\"apName\":\"SJC17-12A-AP100\",\"xFt\":107.7,\"yFt\":18.8,\"zFt\":10,\"uUid\":\"e912b17f-0fdf-4f9a-a182-9f5561cf2a8c\",\"majorId\":4000,\"minorId\":100}]}" as AnyObject?)
        return
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            cmxhfCopyData(information, sourceKey: "macAddress", destinationDictionary: parameters, destinationKey: "macAddress", methodName:#function)
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_USER_LIVE_LOCATION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }

}


