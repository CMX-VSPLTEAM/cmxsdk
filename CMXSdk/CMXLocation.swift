//
//  CMXLocation.swift
//
//  Created by  on 19/10/16
//  Copyright (c) . All rights reserved.
//

import Foundation

public class CMXLocation: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCMXLocationYKey: String = "y"
	internal let kCMXLocationXKey: String = "x"


    // MARK: Properties
	public var y: Float?
	public var x: Float?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		y = json[kCMXLocationYKey].float
		x = json[kCMXLocationXKey].float

    }

    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    required public init?(map: Map){

    }

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
		y <- map[kCMXLocationYKey]
		x <- map[kCMXLocationXKey]

    }
}
