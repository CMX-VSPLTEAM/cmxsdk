//
//  CMX SensorConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXSensorConfigurations: NSObject {
    
    /// initialize instance
    static let shared : CMXSensorConfigurations = {
        let instance = CMXSensorConfigurations()
        return instance
    }()
   
    override init() {
    }

    var magnetometerUpdateInterval = 0.1
    var gyroScopeUpdateInterval = 0.1
    var accelerometerUpdateInterval = 0.1
    var deviceMotionUpdateInterval = 0.1
}
