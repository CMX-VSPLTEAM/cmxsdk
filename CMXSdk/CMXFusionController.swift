//
//  CMXFusionController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


// Content Description : This class deals with anything related to CMX Server

import Foundation

/// Call back handlers , block based
public typealias CMXFSuccessUpdateBlock = (_ update:CMXLocationData) ->()
public typealias CMXFFailureUpdateBlock = (_ error:NSError?) ->()

/// Manager class dealling with user location update , it usually combines bvar motion sensors observations and location from CMX server , and after performing some data optimisation algorithm return most accurate location data.
class CMXFusionController: NSObject {
    
    /// <#Description#>
    private var updateLocation : CMXFSuccessUpdateBlock?
    
    /// <#Description#>
    private var failureUpdateLocation : CMXFFailureUpdateBlock?
    
    /// <#Description#>
    private var macAddress = ""
    
    /// <#Description#>
    private var cmxLocationsValues = [CMXLocationData]()
    
    /// <#Description#>
    private var accelerationValues = [CMXAcceleration]()
    
    /// <#Description#>
    private var magneticFieldValues = [CMXMagneticField]()
    
    /// <#Description#>
    private var rotationValues = [CMXRotation]()
    
    /// <#Description#>
    private var attitudeValues = [CMXAttitude]()
    
    /// <#Description#>
    private var altitudeValues = [CMXAltitude]()

    /// <#Description#>
    private var pedometerValues = [CMXPedometerData]()
    
    /// <#Description#>
    private var motionActivityValues = [CMXMotionActivity]()
    
    /// <#Description#>
    private var nearByBlueToothDevices = [CMXBluetoothPeripheral]()
    
    /// <#Description#>
    private var updateInterval = 0.5
    
    /// <#Description#>
    private var lastCMXLocationValue:CMXLocationData?
    
    /// <#Description#>
    private var lastMacAddress:String?
    
    /// <#Description#>
    private var ableToGetLocationUpdate = true
    
    /// <#Description#>
    private var floorPath : CMXFloorPath?
    
    /// initialize instance
    static let shared : CMXFusionController = {
        let instance = CMXFusionController()
        return instance
    }()
    
    override init() {
    }
    
    /// <#Description#>
    public func logOut() {
        stopLocationUpdates()
        clearReferences()
        CMXCMXLocationController.shared.logOut()
        CMXSensorController.shared.logOut()
    }
    
    
    /// <#Description#>
    func clearReferences() {
        self.updateLocation = nil
        self.failureUpdateLocation = nil
        self.cmxLocationsValues.removeAll()
        self.nearByBlueToothDevices.removeAll()
        self.cmxLocationsValues.removeAll()
        self.accelerationValues.removeAll()
        self.magneticFieldValues.removeAll()
        self.rotationValues.removeAll()
        self.attitudeValues.removeAll()
        self.altitudeValues.removeAll()
        self.pedometerValues.removeAll()
        self.motionActivityValues.removeAll()
    }
    
    
    /// <#Description#>
    ///
    /// - parameter macAddress:     <#macAddress description#>
    /// - parameter updateInterval: <#updateInterval description#>
    /// - parameter update:         <#update description#>
    /// - parameter failure:        <#failure description#>
    func startLocationUpdates(_ macAddress:String,_ updateInterval: Double = 0.2,_ update:@escaping CMXFSuccessUpdateBlock,failure:@escaping CMXFFailureUpdateBlock) {
        if CMXAuthentication.shared.isLoggedIn {
            CMXAppConfigurations.shared.userLocationCallBackUpdateInterval = updateInterval
            self.macAddress = macAddress
            self.updateLocation = update
            self.failureUpdateLocation = failure
            self.updateInterval = updateInterval
            CMXInternalConfigurations.shared.observationStartDate = NSDate()
            ableToGetLocationUpdate = true
            updateInstantlyFromCache()
            
            /// <#Description#>
            func reScheduleUpdate(){
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXFusionController.fetchLocationAndUpdate), object: nil)
                self.perform(#selector(CMXFusionController.fetchLocationAndUpdate), with: nil, afterDelay: TimeInterval(CMXAppConfigurations.shared.userLocationCallBackUpdateInterval))
            }
            
            reScheduleUpdate()
        }else{
            failure(NSError(domain: "Invalid", code: 0, userInfo: ["description":"please login first"]))
        }
    }
    
    func updateInstantlyFromCache(){
        if lastCMXLocationValue != nil {
            if lastCMXLocationValue!.capturedAt.compare(Date(timeIntervalSinceNow: -240)) == ComparisonResult.orderedDescending {
                if lastMacAddress != nil {
                    if lastMacAddress == macAddress {
                        self.cmxLocationsValues.removeAll()
                        self.cmxLocationsValues.append(lastCMXLocationValue!)
                        updatelocationContinouslyByApplyingFusionAlgorithm()
                    }
                }
            }
        }
    }
    
    func exportCollectedDataToFile() {
        if CMXAppConfigurations.shared.writeDataToCSVFile {
            var folderName = ""
            for i in 1...1024 {
                let path = "\(cmxhfDocumentsDirectory())/\(i)"
                if FileManager.default.fileExists(atPath: path, isDirectory: nil){
                    continue
                }else{
                  folderName = "\(i)"
                    break
                }
            }
            CMXDataExportController.sharedInstance.exportCMXLocationValues(collection: cmxLocationsValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportAccelerationValues(collection: accelerationValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportMagneticFieldValues(collection: magneticFieldValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportRotationValues(collection: rotationValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportAttitudeValues(collection: attitudeValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportAltitudeValues(collection: altitudeValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportPedometerValues(collection: pedometerValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportActivityValues(collection: motionActivityValues,folderName: folderName)
            CMXDataExportController.sharedInstance.exportBluetoothMarkerValues(collection: nearByBlueToothDevices,folderName: folderName)
        }
    }
    
    /// <#Description#>
    func stopLocationUpdates() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        exportCollectedDataToFile()
        CMXCMXLocationController.shared.stopCMXLocationUpdates()
        CMXSensorController.shared.stopMotionUpdates()
        CMXSensorController.shared.stopActivityUpdates()
        CMXSensorController.shared.stopScanningBluetoothDevice()
        clearReferences()
    }
    
    /// <#Description#>
    func fetchLocationAndUpdate(){
        startCMXLocationUpdates()
        startSensorUpdates()
        startMotionActivityUpdates()
        startScanningNearbyBlueToothDevices()
        updatelocationContinouslyByApplyingFusionAlgorithm()
        loadFloorPath()
    }
    
    func startScanningNearbyBlueToothDevices(){
        nearByBlueToothDevices.removeAll()
        CMXSensorController.shared.startScanningBluetoothDevice({[weak self] (device) in
            guard let `self` = self else { return }
            cmxhfLog(data: "bluetooth device found : \(cmxhfSafeString(device.peripheral?.name))")
            self.nearByBlueToothDevices.append(device)
            }) { [weak self] (error) in guard let `self` = self else { return }
                cmxhfLogErrorIfRequired(error: error)
        }
    }
    
    
    func startCMXLocationUpdates(){
        // fetch location update from server
        CMXCMXLocationController.shared.startCMXLocationUpdates(self.macAddress, {[weak self] (cmxLocation) in guard let `self` = self else { return }
            if self.cmxLocationsValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                self.cmxLocationsValues.removeFirst()
            }
            self.cmxLocationsValues.append(cmxLocation)
            self.ableToGetLocationUpdate = true
        }) { [weak self] (error) in guard let `self` = self else { return }
            self.ableToGetLocationUpdate = false
            cmxhfLogErrorIfRequired(error: error)
        }
    }
    
    func startSensorUpdates(){
        // fetch motion updates from device
        CMXSensorController.shared.startMotionUpdates({[weak self] (magneticField) in guard let `self` = self else { return }
            if self.magneticFieldValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                self.magneticFieldValues.removeFirst()
            }
            self.magneticFieldValues.append(magneticField)
            }, {[weak self] (acceleration) in guard let `self` = self else { return }
                if self.accelerationValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                    self.accelerationValues.removeFirst()
                }
                self.accelerationValues.append(acceleration)
            }, {[weak self] (rotation) in guard let `self` = self else { return }
                if self.rotationValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                    self.rotationValues.removeFirst()
                }
                self.rotationValues.append(rotation)
        }) {[weak self] (attitude) in guard let `self` = self else { return }
            if self.attitudeValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                self.attitudeValues.removeFirst()
            }
            self.attitudeValues.append(attitude)
        }
    }
    
    func startMotionActivityUpdates(){
        // fetch activity updates from device
        CMXSensorController.shared.startUpdatingActivities({[weak self] (altitude) in guard let `self` = self else { return }
            if self.altitudeValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                self.altitudeValues.removeFirst()
            }
            self.altitudeValues.append(altitude)
            }, {[weak self] (pedometerData) in guard let `self` = self else { return }
                if self.pedometerValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                    self.pedometerValues.removeFirst()
                }
                self.pedometerValues.append(pedometerData)
            }) {[weak self] (motionActivity) in guard let `self` = self else { return }
                if self.motionActivityValues.count > CMXAppConfigurations.shared.internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                    self.motionActivityValues.removeFirst()
                }
                self.motionActivityValues.append(motionActivity)
        }
    }
    
    func loadFloorPath() {
        CMXCommunication.shared.getMapFloorPathFromCMX(["":""]) { (response) in
            if response is String {
                let jsonString = response as! String
                self.floorPath = CMXFloorPath(JSONString: jsonString)
                cmxhfLog(data: "FLOOR PATH")
                cmxhfLog(data: self.floorPath)
            }
        }
    }
    
    func updatelocationContinouslyByApplyingFusionAlgorithm() {
        /*
         *
         *
         *
         *       CMX FUSION ALGORITHM
         *       We have to take motion observations , fiter out noisy data , do some calculation , optimisation ...etc
         *
         *
         *
         */
        if ableToGetLocationUpdate == false {
            self.failureUpdateLocation?(NSError(domain: "Invalid", code: 0, userInfo: ["description":"device can not be located at this moment"]))
        }
        if let cmxLocation = cmxLocationsValues.last {
            lastCMXLocationValue = cmxLocation
            lastMacAddress = macAddress
            self.updateLocation?(cmxLocation)
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXFusionController.updatelocationContinouslyByApplyingFusionAlgorithm), object: nil)
        self.perform(#selector(CMXFusionController.updatelocationContinouslyByApplyingFusionAlgorithm), with: nil, afterDelay: updateInterval)
    }
}
