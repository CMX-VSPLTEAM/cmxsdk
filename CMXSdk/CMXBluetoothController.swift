//
//  CMXBluetoothController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreBluetooth

public typealias CMXBCCompletionBlock = (_ bluetoothDevice:CMXBluetoothPeripheral) ->()
public typealias CMXBCFailureBlock = (_ error:NSError) ->()

/// Manager class dealling with bluetooth
class CMXBluetoothController: NSObject , CBCentralManagerDelegate {
    
    /// initialize instance
    static let shared : CMXBluetoothController = {
        let instance = CMXBluetoothController()
        return instance
    }()
    
    private var manager : CBCentralManager?
    var completionBlock : CMXBCCompletionBlock?
    var failureBlock : CMXBCFailureBlock?
    
    override init() {
    }
    
    func stopScanning() {
        manager?.stopScan()
        clearReferences()
    }
    
    func clearReferences(){
        self.completionBlock = nil
        self.failureBlock = nil
    }
    
    func scanForPeripherals(completion:@escaping CMXBCCompletionBlock,failure:@escaping CMXBCFailureBlock) {
        manager = CBCentralManager()
        manager?.delegate = self
        self.completionBlock = completion
        self.failureBlock = failure
        if manager?.state == .poweredOn {
            manager?.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        let bluetoothMarker = CMXBluetoothPeripheral(peripheral: peripheral, rssi: RSSI, advertisementData: advertisementData)
        self.completionBlock?(bluetoothMarker)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch(central.state)
        {
        case.unsupported:
            cmxhfLog(data:"BLE is not supported")
        case.unauthorized:
            cmxhfLog(data: "BLE is unauthorized")
        case.unknown:
            cmxhfLog(data: "BLE is Unknown")
        case.resetting:
            cmxhfLog(data: "BLE is Resetting")
        case.poweredOff:
            cmxhfLog(data: "BLE service is powered off")
        case.poweredOn:
            cmxhfLog(data: "BLE service is powered on")
            cmxhfLog(data: "Start Scanning")
            manager?.scanForPeripherals(withServices: nil, options: nil)
        }
    }
}

