//
//  CMXMapInfo.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold map Info
public class CMXMapInfo: CMXCapturableData {
    /// <#Description#>
    public var floorDimension : CMXFloorDimension

    /// <#Description#>
    public var floorImage : CMXFloorImage

    /// <#Description#>
    public var floorRefId : String
    
    /// <#Description#>
    public var serverTimeStamp : String


    /// <#Description#>
    ///
    /// - parameter floorDimension:  <#floorDimension description#>
    /// - parameter floorImage:      <#floorImage description#>
    /// - parameter floorRefId:      <#floorRefId description#>
    /// - parameter serverTimeStamp: <#serverTimeStamp description#>
    ///
    /// - returns: <#return value description#>
    init(floorDimension:CMXFloorDimension,floorImage:CMXFloorImage,floorRefId:String,serverTimeStamp:String) {
        self.floorDimension = floorDimension
        self.floorImage = floorImage
        self.floorRefId = floorRefId
        self.serverTimeStamp = serverTimeStamp
    }
}
