//
//  CMXSdk.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */




import Foundation
import UIKit

public class CMXSdk: NSObject {
    /// Shared instance
    public static let shared : CMXSdk = {
        let instance = CMXSdk()
        return instance
    }()
    
    override init() {
    }
}

// MARK: - CMX Server Specific
extension CMXSdk {
    /// Authenticate CMX server
    ///
    /// - parameter ip:         ip address of server
    /// - parameter name:       name (it can be any name, user internally for tagging)
    /// - parameter userName:   userName to login with
    /// - parameter password:   password to authenticate login
    /// - parameter completion: completion callback
    /// - parameter failure:    failure callback
    public func authenticate(ip:String,name:String,userName:String,password:String,completion:@escaping CMXASuccessBlock,failure:@escaping CMXAFailureBlock) {
        CMXCommunication.shared.authenticate(ip: ip, name: name, userName: userName, password: password, completion: completion, failure: failure)
    }
    
    /// Logout
    /// Calling this will clear everything and stop any already running fetches and updates
    public func logout(){
        CMXCommunication.shared.logOut()
        CMXFusionController.shared.logOut()
    }
    
    /// IsLoggedIn
    public func IsLoggedIn()->Bool{
        return CMXCommunication.shared.IsLoggedIn()
    }
    
    /// activeServer
    /// returns Active Server Object if Exist
    public func activeServer()->CMXServer?{
        return CMXCommunication.shared.activeServer()
    }
    
    /// Start Location Updates of User from CMX Server
    ///
    /// - parameter macAddress: macAddress of user's device
    /// - parameter updateInterval: how frequestly you need location updates
    /// - parameter completion: completion block
    public func startLocationUpdates(_ macAddress:String,_ updateInterval: Double = 5,_ update:@escaping CMXFSuccessUpdateBlock,failure:@escaping CMXFFailureUpdateBlock) {
        assert(updateInterval>0,"updateInterval should be greater than 0")
        assert(CMXAuthentication.shared.isLoggedIn,"please authenticate first before using any services")
        CMXFusionController.shared.startLocationUpdates(macAddress, updateInterval, update, failure: failure)
    }
    
    /// This load the image resources like map floor image
    ///
    /// - parameter imageName:  name of image
    /// - parameter completion: completion
    public func getImageData(_ imageName:String,_ completion:@escaping CMXACCompletionBlockForFile) {
        CMXCommunication.shared.getImageData(imageName, completion)
    }
    
    /// <#Description#>
    public func stopLocationUpdates() {
        CMXFusionController.shared.stopLocationUpdates()
    }
    
    /// <#Description#>
    ///
    /// - parameter enabled: <#enabled description#>
    public func setLoggingEnabled(enabled:Bool){
        CMXAppConfigurations.shared.loggingEnabled = enabled
    }
    
    /// <#Description#>
    ///
    /// - parameter enabled: <#enabled description#>
    public func setLocationUpdateInterval(updateInterval:Double){
        assert(updateInterval>0,"updateInterval should be greater than 0")
        CMXAppConfigurations.shared.userLocationCallBackUpdateInterval = updateInterval
    }
    
}
