//
//  CMXMapCoordinate.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold map coordinate
public class CMXMapCoordinate: CMXCapturableData {
    
    /// <#Description#>
    public var x = 0.0
    /// <#Description#>
    public var y = 0.0
    /// <#Description#>
    public var z = 0.0
    /// <#Description#>
    public var unit = "FEET"
    /// <#Description#>
    public var confidenceFactor = ""
    
    /// <#Description#>
    ///
    /// - parameter x:    <#x description#>
    /// - parameter y:    <#y description#>
    /// - parameter z:    <#z description#>
    /// - parameter unit: <#unit description#>
    ///
    /// - returns: <#return value description#>
    init(x:Double,y:Double,z:Double,unit:String) {
        self.x = x
        self.y = y
        self.z = y
        self.unit = unit
    }
}
