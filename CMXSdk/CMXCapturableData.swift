//
//  CMXCapturableData.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// <#Description#>
public class CMXCapturableData: NSObject {
    /// <#Description#>
    public var capturedAt = Date()

    public var toRecord = true

    /// <#Description#>
    public var observationDate = CMXAppConfigurations.shared.internalConfigurations.observationStartDate

    /// <#Description#>
    ///
    /// - returns: <#return value description#>
    override init() {
    }
}
