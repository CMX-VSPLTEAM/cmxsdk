//
//  CMXSensorController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion


/// Call back handlers , block based
public typealias CMXCLCMFCompletionBlock = (_ magneticField:CMXMagneticField) ->()
public typealias CMXCLCACCompletionBlock = (_ acceleration:CMXAcceleration) ->()
public typealias CMXCLCRCompletionBlock = (_ rotation:CMXRotation) ->()
public typealias CMXCLCATCompletionBlock = (_ attitude:CMXAttitude) ->()
public typealias CMXCLCALCompletionBlock = (_ altitude:CMXAltitude) ->()
public typealias CMXCLCPDCompletionBlock = (_ pedometer:CMXPedometerData) ->()
public typealias CMXCLCMACompletionBlock = (_ motionActivity:CMXMotionActivity) ->()
public typealias CMXCLCMASCompletionBlock = (_ motionActivities:[CMXMotionActivity]?) ->()

/// Manager class dealling with every type of motion events
class CMXSensorController: NSObject {
    
    /// initialize instance
    static let shared : CMXSensorController = {
        let instance = CMXSensorController()
        return instance
    }()
    
    override init() {
    }
    
    var motionManager: CMMotionManager?
    /// <#Description#>
    
    var pedometer: CMPedometer?
    /// <#Description#>
    
    var altimeter: CMAltimeter?
    /// <#Description#>
    
    var motionActivityManager: CMMotionActivityManager?
    /// <#Description#>
    
    public func logOut() {
        stopMotionUpdates()
        stopActivityUpdates()
        stopScanningBluetoothDevice()
    }
}

// MARK: - Bluetooth Controller Specific

extension CMXSensorController {
    func stopScanningBluetoothDevice() {
        CMXBluetoothController.shared.stopScanning()
    }
    
    func startScanningBluetoothDevice(_ completion: @escaping CMXBCCompletionBlock, failure: @escaping CMXBCFailureBlock) {
        CMXBluetoothController.shared.scanForPeripherals(completion: completion, failure: failure)
    }
}


// MARK: - Motion Controller Specific

extension CMXSensorController {
    
    func stopMotionUpdates() {
        if motionManager != nil{
            if motionManager!.isGyroActive {
                motionManager?.stopGyroUpdates()
            }
            if motionManager!.isAccelerometerActive {
                motionManager?.stopAccelerometerUpdates()
            }
            if motionManager!.isMagnetometerActive{
                motionManager?.stopMagnetometerUpdates()
            }
            if motionManager!.isDeviceMotionActive {
                motionManager?.stopDeviceMotionUpdates()
            }
            motionManager = nil;
        }
    }
    
    func isMotionManagerRunning () -> Bool {
        return motionManager != nil ? true : false;
    }
    
    /**
     call this method to get live motion updates
     
     - parameter completionMF: Magenetic field update callback
     - parameter completionAC: Accelerometer update callback
     - parameter completionR:  Gyroscope update callback
     - parameter completionAT: Attitude update callback
     */
    func startMotionUpdates(_ completionMF: CMXCLCMFCompletionBlock?,_ completionAC:CMXCLCACCompletionBlock?,_ completionR: CMXCLCRCompletionBlock?,_ completionAT:CMXCLCATCompletionBlock?) {
        
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        
        //set motion interval
        motionManager?.magnetometerUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.magnetometerUpdateInterval
        motionManager?.gyroUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.gyroScopeUpdateInterval
        motionManager?.accelerometerUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.accelerometerUpdateInterval
        motionManager?.deviceMotionUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.deviceMotionUpdateInterval
        
        func startAccelerometerUpdates(){
            if motionManager!.isAccelerometerAvailable {
                motionManager?.startAccelerometerUpdates(to: OperationQueue.main, withHandler: {[weak self] (object, error) in guard let `self` = self else { return }
                    self.motionManager?.accelerometerUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.accelerometerUpdateInterval
                    if error != nil {
                        cmxhfLogErrorIfRequired(error: error as NSError?)
                    }else if  object != nil {
                        let ax = object?.acceleration.x ?? 0.0
                        let ay = object?.acceleration.y ?? 0.0
                        let az = object?.acceleration.z ?? 0.0
                        let accelerate = CMXAcceleration(x: ax, y: ay, z: az)
                        DispatchQueue.main.asyncAfter(deadline:.now()) {
                            completionAC?(accelerate)
                        }
                    }
                })
            }
        }
        
        func startGyroUpdates(){
            if motionManager!.isGyroAvailable {
                motionManager?.startGyroUpdates(to: OperationQueue.main, withHandler: {[weak self] (object, error) in guard let `self` = self else { return }
                    self.motionManager?.gyroUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.gyroScopeUpdateInterval
                    if error != nil {
                        cmxhfLogErrorIfRequired(error: error as NSError?)
                    }else if  object != nil {
                        let rx = object?.rotationRate.x ?? 0.0
                        let ry = object?.rotationRate.y ?? 0.0
                        let rz = object?.rotationRate.z ?? 0.0
                        let rotation = CMXRotation(x: rx, y: ry, z: rz, rotationRate: object?.rotationRate)
                        DispatchQueue.main.asyncAfter(deadline:.now()) {
                            completionR?(rotation)
                        }
                    }
                })
            }
        }
        
        
        func startMagnetometerUpdates(){
            if motionManager!.isMagnetometerAvailable {
                //start magneto meter readings.. Since Device motion update din't return megntic field data
                motionManager?.startMagnetometerUpdates(to: OperationQueue.main) {[weak self](magneticField, error) in guard let `self` = self else { return }
                    self.motionManager?.magnetometerUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.magnetometerUpdateInterval
                    if error != nil {
                        cmxhfLogErrorIfRequired(error: error as NSError?)
                    }else if magneticField != nil {
                        let mfx = magneticField?.magneticField.x ?? 0.0
                        let mfy = magneticField?.magneticField.y ?? 0.0
                        let mfz = magneticField?.magneticField.z ?? 0.0
                        let magneticField = CMXMagneticField(x: mfx, y: mfy, z: mfz)
                        DispatchQueue.main.asyncAfter(deadline:.now()) {
                            completionMF?(magneticField)
                        }
                    }
                }
            }
        }
        
        func startDeviceMotionUpdates(){
            if motionManager!.isDeviceMotionAvailable {
                motionManager?.startDeviceMotionUpdates(to: OperationQueue.main) {[weak self] (object, error) in guard let `self` = self else { return }
                    self.motionManager?.deviceMotionUpdateInterval = CMXAppConfigurations.shared.sensorConfigurations.deviceMotionUpdateInterval
                    if error != nil {
                        cmxhfLogErrorIfRequired(error: error as NSError?)
                    }else if  object != nil {
                        let yaw = object?.attitude.yaw ?? 0.0
                        let roll = object?.attitude.roll ?? 0.0
                        let pitch = object?.attitude.pitch ?? 0.0
                        let attitude = CMXAttitude(yaw: yaw, roll: roll, pitch: pitch)
                        DispatchQueue.main.asyncAfter(deadline:.now()) {
                            completionAT?(attitude)
                        }
                    }
                }
            }
        }

        startAccelerometerUpdates()
        startGyroUpdates()
        startMagnetometerUpdates()
        startDeviceMotionUpdates()
        
    }
    
}


// MARK: - Activity Controller Specific

extension CMXSensorController {
    /**
     Start Activity update to main queue
     
     - parameter completionAL: Altitude value update callback
     - parameter completionPM: Pedometer value update callback
     - parameter completionMA: Motion Activity update callback
     */
    func startUpdatingActivities(_ completionAL: CMXCLCALCompletionBlock?,_ completionPM: CMXCLCPDCompletionBlock?,_ completionMA: CMXCLCMACompletionBlock?) {
        
        pedometer = CMPedometer()
        altimeter = CMAltimeter()
        motionActivityManager = CMMotionActivityManager()
        if CMAltimeter.isRelativeAltitudeAvailable(){
            altimeter?.startRelativeAltitudeUpdates(to: OperationQueue.main) {[weak self]
                (altitudeData, error) in guard let `self` = self else { return }
                if error != nil {
                    cmxhfLog(data: "ERROR ALTIMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        if altitudeData != nil {
                            completionAL?(CMXAltitude(altitide: altitudeData!))
                        }
                    }
                }
            }
        }
        
        func startPedimeter(){
            pedometer?.startUpdates(from: Date() as Date) {[weak self]
                (pedometerData, error) in guard let `self` = self else { return }
                if error != nil {
                    cmxhfLog(data: "ERROR PEDOMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        if pedometerData != nil {
                            completionPM?(CMXPedometerData(pedometer: pedometerData!))
                        }
                    }
                }
            }
        }
        if #available(iOS 10.0, *) {
            if CMPedometer.isPedometerEventTrackingAvailable() {
                startPedimeter()
            }
        } else {
            startPedimeter()
        }
        
        if CMMotionActivityManager.isActivityAvailable() {
            motionActivityManager?.startActivityUpdates(to: OperationQueue.main) {[weak self]
                activityData in guard let `self` = self else { return }
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if activityData != nil{
                        completionMA?(CMXMotionActivity(motionActivity: activityData!))
                    }
                }
            }
        }
    }
    
    /// <#Description#>
    func stopActivityUpdates() {
        pedometer = nil
        altimeter = nil
        motionActivityManager = nil
    }
    
    /// <#Description#>
    ///
    /// - returns: <#return value description#>
    func isActivityUpdatesRunning () -> Bool {
        return ((pedometer != nil ? true : false) || (altimeter != nil ? true : false) || (motionActivityManager != nil ? true : false))
    }
    
    
    /**
     Fetch history from MotionActivity
     
     - parameter completionMAS: MotionActivities update callback
     */
    func fetchHistoricalMotionActivities(_ completionMAS: CMXCLCMASCompletionBlock?) {
        let oneWeekTimeInterval = 24 * 3600 as TimeInterval
        let fromDate = Date(timeIntervalSinceNow: -oneWeekTimeInterval)
        let toDate = Date()
        motionActivityManager?.queryActivityStarting(from: fromDate as Date,to: toDate as Date, to: OperationQueue.main) {[weak self]
            (activities, error) in guard let `self` = self else { return }
            if error != nil {
                cmxhfLog(data: "ERROR QUERYING ACTIVITIES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            }else{
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    var motionActivities = [CMXMotionActivity]()
                    if activities != nil {
                        for activity in activities!.reversed() {
                            motionActivities.append(CMXMotionActivity(motionActivity: activity))
                        }
                    }
                    completionMAS?(motionActivities)
                }
            }
        }
    }
    
    
    /**
     Fetch pedometer data between provide data ranges
     
     - parameter from:         Start date
     - parameter to:           End data
     - parameter completionPD: Pedomoeter data update callback
     */
    func fetchPedometerData(_ from:Date,to:Date,_ completionPD: CMXCLCPDCompletionBlock?) {
        pedometer?.queryPedometerData(from: from as Date, to:to as Date) {[weak self]
            (pedometerData, error) -> Void in guard let `self` = self else { return }
            if error != nil {
                cmxhfLog(data: "ERROR QUERYING PEDOMETER DETAILS , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            } else {
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if pedometerData != nil {
                        completionPD?(CMXPedometerData(pedometer: pedometerData!))
                    }
                }
            }
        }
    }
    
}
