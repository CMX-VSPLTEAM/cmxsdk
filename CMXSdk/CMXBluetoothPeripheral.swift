//
//  CMXBluetoothPeripheral.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreBluetooth

/// Data modal to raw floor path nodes
public class CMXBluetoothPeripheral: CMXCapturableData {

    /// <#Description#>
    let peripheral : CBPeripheral?
    
    /// <#Description#>
    let rssi : NSNumber?

    /// <#Description#>
    let advertisementData : [String : Any]?

    /// <#Description#>
    ///
    /// - parameter peripheral: <#peripheral description#>
    ///
    // - returns: <#return value description#>
    init(peripheral:CBPeripheral,rssi:NSNumber,advertisementData:[String : Any]?) {
        self.peripheral = peripheral
        self.rssi = rssi
        self.advertisementData = advertisementData
    }

}

