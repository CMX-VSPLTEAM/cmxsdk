//
//
//  CMXDataExportController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXDataExportController: NSObject {
    
    /// <#Description#>
    static let sharedInstance : CMXDataExportController = {
        let instance = CMXDataExportController()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    /// <#Description#>
    ///
    /// - parameter date: <#date description#>
    ///
    /// - returns: <#return value description#>
    func dateStringForDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date).replacingOccurrences(of: ",", with: " ")
    }
    
    func currentTimeStamp() -> Int64{
        return Int64(NSDate().timeIntervalSince1970*1000)
    }
    
    /// <#Description#>
    func exportAccelerationValues(collection:[CMXAcceleration],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Acceleration.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","X (g) ","Y (g) ","Z (g)"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let xValues = NSMutableArray()
        let yValues = NSMutableArray()
        let zValues = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Acceleration")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    xValues.add("\(object.x)")
                    yValues.add("\(object.y)")
                    zValues.add("\(object.z)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,xValues,yValues,zValues])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportRotationValues(collection:[CMXRotation],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Rotation.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","X  (radian/sec) ","Y  (radian/sec) ","Z  (radian/sec)"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let xValues = NSMutableArray()
        let yValues = NSMutableArray()
        let zValues = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Rotation")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    xValues.add("\(object.x)")
                    yValues.add("\(object.y)")
                    zValues.add("\(object.z)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,xValues,yValues,zValues])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportMagneticFieldValues(collection:[CMXMagneticField],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)MagneticField.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","X  (μT)","Y  (μT)","Z  (μT)"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let xValues = NSMutableArray()
        let yValues = NSMutableArray()
        let zValues = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Magnetic Field")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    xValues.add("\(object.x)")
                    yValues.add("\(object.y)")
                    zValues.add("\(object.z)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,xValues,yValues,zValues])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportAttitudeValues(collection:[CMXAttitude],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Attitude.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","Yaw  (Degree)","Pitch  (Degree)","Roll  (Degree)"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let xValues = NSMutableArray()
        let yValues = NSMutableArray()
        let zValues = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Attitude")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    xValues.add("\(object.yaw)")
                    yValues.add("\(object.pitch)")
                    zValues.add("\(object.roll)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,xValues,yValues,zValues])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportActivityValues(collection:[CMXMotionActivity],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Activity.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","Activity Type"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let activityType = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Activity")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    if object.motionActivity.running {
                        activityType.add("Running")
                    } else if object.motionActivity.cycling {
                        activityType.add("Cycling")
                    } else if object.motionActivity.walking {
                        activityType.add("Walking")
                    }else if object.motionActivity.automotive {
                        activityType.add("Automotive")
                    }else if object.motionActivity.stationary {
                        activityType.add("Stationary")
                    }else {
                        activityType.add("Unknown")
                    }
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,activityType])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportPedometerValues(collection:[CMXPedometerData],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Pedometer.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","Steps","Floors","Distance (meters)"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let steps = NSMutableArray()
        let floors = NSMutableArray()
        let distance = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Pedometer")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    steps.add("\(cmxhfSafeInt(object.pedometer.numberOfSteps, alternate: 0))")
                    floors.add("\(cmxhfSafeInt(object.pedometer.floorsAscended, alternate: 0))")
                    distance.add("\(cmxhfSafeDouble(object.pedometer.distance, alternate: 0))")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,steps,floors,distance])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportAltitudeValues(collection:[CMXAltitude],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)Altitude.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","Relative Altitude"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let relativeAltitudeArray = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Altitude")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    relativeAltitudeArray.add("\(object.altitide.relativeAltitude)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative ,relativeAltitudeArray])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportCMXLocationValues(collection:[CMXLocationData],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)CMXLocationUpdate.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","Server TimeStamp","X","Y","Z","Floor Id"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let serverTimeStamp = NSMutableArray()
        let xValues = NSMutableArray()
        let yValues = NSMutableArray()
        let zValues = NSMutableArray()
        let floorId = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("location (feet)")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    serverTimeStamp.add("\(object.mapInfo.serverTimeStamp)")
                    xValues.add("\(object.mapCoordinate.x)")
                    yValues.add("\(object.mapCoordinate.y)")
                    zValues.add("\(object.mapCoordinate.z)")
                    floorId.add("\(object.mapInfo.floorRefId)")
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,serverTimeStamp,xValues,yValues,zValues,floorId])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
    
    /// <#Description#>
    func exportBluetoothMarkerValues(collection:[CMXBluetoothPeripheral],folderName:String)->String{
        let fileNamePrefix = "\(currentTimeStamp())_"
        let fileName = "\(fileNamePrefix)NearByBluetoothPeripheral.csv"
        let keys = ["Observation Type","TimeStamp (ms)","Timestamp","Relative Timestamp (ms)","RSSI","UUID"]
        let observationTypeArray = NSMutableArray()
        let timeStampMS = NSMutableArray()
        let timeStamp = NSMutableArray()
        let timeStampRelative = NSMutableArray()
        var lastTimeStamp : NSDate!
        let rssi = NSMutableArray()
        let uuid = NSMutableArray()
        if collection.count > 0 {
            for i in 0...collection.count-1 {
                let object = collection[i]
                if object.toRecord {
                    observationTypeArray.add("Bluetooth peripherals")
                    timeStampMS.add("\(String(format: "%ld", object.capturedAt.timeIntervalSince1970*1000))")
                    timeStamp.add("\(object.capturedAt.cmxhfToStringCustom())")
                    if lastTimeStamp != nil {
                        let elapsed = object.capturedAt.timeIntervalSince(lastTimeStamp as Date)
                        timeStampRelative.add(String(format: "%.0f", elapsed*1000))
                        lastTimeStamp = object.capturedAt as NSDate?
                    }else{
                        lastTimeStamp = object.capturedAt as NSDate?
                        timeStampRelative.add("0")
                    }
                    rssi.add(cmxhfSafeString(object.rssi))
                    uuid.add(cmxhfSafeString(object.peripheral?.identifier.uuidString))
                }
            }
        }
        if observationTypeArray.count > 0 {
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,timeStampMS,timeStamp,timeStampRelative,rssi,uuid])
        }else{
            return CMXCSVExporter.exportToCsv(fileName as NSString,folderName as NSString, keyArray: keys as NSArray, variableArray: [["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"],["No Record Found"]])
        }
    }
}
