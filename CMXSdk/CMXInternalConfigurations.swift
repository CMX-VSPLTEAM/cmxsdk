//
//  CMXInternalConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Manager class to hold various runtime configurations
class CMXInternalConfigurations: NSObject {
    
    /// initialize instance
    static let shared : CMXInternalConfigurations = {
        let instance = CMXInternalConfigurations()
        return instance
    }()
    
    override init() {
    }
    
    public var observationStartDate : NSDate?
    public var numberOfObservationsRequiredForFusionAlgorithm = 500
}
