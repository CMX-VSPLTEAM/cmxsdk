//
//  CMXFloorImage.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold floor image information
public class CMXFloorImage: CMXCapturableData {
    /// <#Description#>
    public var colorDepth = 0
    /// <#Description#>
    public var height = 0
    /// <#Description#>
    public var imageName = ""
    /// <#Description#>
    public var maxResolution = 0
    /// <#Description#>
    public var size = 0
    /// <#Description#>
    public var width = 0
    /// <#Description#>
    public var zoomLevel = 0
    
    /// <#Description#>
    ///
    /// - parameter colorDepth:    <#colorDepth description#>
    /// - parameter height:        <#height description#>
    /// - parameter imageName:     <#imageName description#>
    /// - parameter maxResolution: <#maxResolution description#>
    /// - parameter size:          <#size description#>
    /// - parameter width:         <#width description#>
    /// - parameter zoomLevel:     <#zoomLevel description#>
    ///
    /// - returns: <#return value description#>
    init(colorDepth:Int,height:Int,imageName:String,maxResolution:Int,size:Int,width:Int,zoomLevel:Int) {
        self.colorDepth = colorDepth
        self.height = height
        self.imageName = imageName
        self.maxResolution = maxResolution
        self.size = size
        self.width = width
        self.zoomLevel = zoomLevel
    }
}
